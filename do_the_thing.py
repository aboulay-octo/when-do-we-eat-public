from __future__ import division

import os
import sys
import uuid
from datetime import datetime
if os.path.exists('libs.zip'):
    sys.path.insert(0, 'libs.zip')

from cassandra.cluster import Cluster

from pyspark import SparkConf
import pyspark_cassandra
import pytz


def treat_cqlenv():
    sql_addr = os.environ.get("CQL_ADDR")
    sql_addr = None if sql_addr is None else sql_addr.split(" ")
    return sql_addr


def connect_cassandra():
    sql_addr = treat_cqlenv()
    print "CQL: Connecting on " + str(sql_addr) + "..."
    while True:
        try:
            cluster = Cluster(sql_addr)
            session = cluster.connect()
            print "CQL: Connection Done"
            break
        except KeyboardInterrupt:
            exit(1)
        except Exception:
            error = sys.exc_info()
            typerr = u"%s" % (error[0])
            msgerr = u"%s" % (error[1])
            print "Error: " + typerr + ": " + msgerr
            print "CQL Error: Retrying..."
    return session


def create_tresult(keyspace, session):
    table = "result"
    session.execute("""
        CREATE TABLE IF NOT EXISTS {keyspace}.{table} (
            id text PRIMARY KEY,
            choice text,
            percent float,
            date timestamp
        );
    """.format(keyspace=keyspace, table=table))
    print "Spark: CQL: Created table: {!r}.{!r}".format(keyspace, table)


def init():
    keyspace = os.getenv("KEYSPACE", "basic")
    session = connect_cassandra()
    create_tresult(keyspace, session)


def run():
    # Initialization Spark Config
    conf = SparkConf()
    conf.setAppName("PySpark Cassandra Statistic Reducer")
    conf.set("spark.cassandra.connection.host",
             os.getenv("CQL_ADDR", "localhost"))
    # Initialization Spark Context
    sc = pyspark_cassandra.CassandraSparkContext(conf=conf)

    # Get the content of a table in a specific keyspace
    base_rdd = sc.cassandraTable(os.getenv("KEYSPACE"), "choice")

    # Count all the choice in the RDD
    # get RDD
    # return int
    full = base_rdd.select("choice").count()
    print "Spark: Total number of row: " + str(full)
    # Count the number of each value in the RDD
    # get RDD
    # return [(key, number), ...]
    parts = base_rdd.select("choice").countByValue().items()

    print "Spark: parts result: " + str(parts)

    date = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

    # Computing RDD
    final_rdd = sc.parallelize([{
        "id": str(uuid.uuid4()),
        "choice": part[0]["choice"],
        "percent": part[1]/full,
        "date": date
        } for part in parts])
    # Saving data in Cassandra Table
    pyspark_cassandra.saveToCassandra(final_rdd, os.getenv("KEYSPACE"), "result")
    print "Done"


def main():
    print "Initialization..."
    init()
    print "Starting spark service"
    run()


if __name__ == "__main__":
    main()
